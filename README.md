# MAST

[![dev](https://img.shields.io/badge/docs-latest-blue?style=for-the-badge&logo=julia)](https://ExpandingMan.gitlab.io/MAST.jl/)
[![build](https://img.shields.io/gitlab/pipeline/ExpandingMan/MAST.jl/master?style=for-the-badge)](https://gitlab.com/ExpandingMan/MAST.jl/-/pipelines)


A Julia wrapper for the Space Telescope Science Institute's [Mikulski Archive for Space
Telescopes](https://mast.stsci.edu/portal/Mashup/Clients/Mast/Portal.html) HTTP API.

Documentation for the archive can be found
[here](https://outerspace.stsci.edu/display/MASTDOCS/Portal+Guide).

Documentation of the API can be found [here](https://mast.stsci.edu/api/v0/index.html).
