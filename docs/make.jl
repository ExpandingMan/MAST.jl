using MAST
using Documenter

DocMeta.setdocmeta!(MAST, :DocTestSetup, :(using MAST); recursive=true)

makedocs(;
    modules=[MAST],
    authors="Expanding Man <savastio@protonmail.com> and contributors",
    repo="https://gitlab.com/ExpandingMan/MAST.jl/blob/{commit}{path}#{line}",
    sitename="MAST.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://ExpandingMan.gitlab.io/MAST.jl",
        edit_link="main",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)
