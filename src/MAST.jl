module MAST

using HTTP, HTTP.URIs, JSON3, Dates


const DEFAULT_URL = URI("https://mast.stsci.edu/api/")


struct MastAPI
    url::URI

    MastAPI(url::URI=DEFAULT_URL) = new(url)
end
MastAPI(url::AbstractString) = MastAPI(URI(url))

function headers(;kw...)
    o = Dict{String,String}("Content-type"=>"application/x-www-form-urlencoded",
                            "Accept"=>"text/plain",
                           )
    merge!(o, Dict(string(k)=>string(v) for (k,w) ∈ kw))
end

urlof_query(api::MastAPI) = joinpath(api.url, "v0/invoke")

function query_raw(api::MastAPI, r::AbstractDict; headers::AbstractDict=headers())
    b = "request="*JSON3.write(r)
    HTTP.post(urlof_query(api), headers, b)
end
function query(api::MastAPI, ::Type{T}, r::AbstractDict; kw...) where {T}
    s = query_raw(api, r; kw...)
    JSON3.read(s.body, T)
end

# this method assumes standard tabular response but not all endpoints return it...
function query(api::MastAPI, ::Nothing, r::AbstractDict;
               attempts_max::Integer=100, attempts_δt::Real=10.0,
               attempts_log::Bool=true,
               kw...)
    for i ∈ 1:attempts_max
        s = query_raw(api, r; kw...).body |> JSON3.read
        if s[:status] == "ERROR"
            @error("MAST internal error", message=s[:msg])
            error("MAST internal error")
        elseif s[:status] == "EXECUTING"
            attempts_log && @info("MAST: still executing after attempt $i; waiting $attempts_δt...")
            sleep(attempts_δt)
        else
            return s[:data]
        end
    end
    error("MAST query failed to complete after maximum attempts $attempts_max")
end
query(api::MastAPI, r::AbstractDict; kw...) = query(api, nothing, r; kw...)

function request(service::AbstractString;
                 params=(;),
                 format::AbstractString="json",
                 data=nothing,
                 filename::Union{Nothing,AbstractString}=nothing,
                 timeout::Union{Integer,Nothing}=nothing,
                 clearcache::Bool=false,
                 removecache::Bool=false,
                 removenullcolumns::Bool=false,
                 page::Integer=1,
                 pagesize::Integer=1000,
                )
    o = Dict("service"=>service,
             "format"=>format,
             "clearcache"=>clearcache,
             "removecache"=>removecache,
             "removenullcolumns"=>removenullcolumns,
             "page"=>page,
             "pagesize"=>pagesize,
            )
    isnothing(params) || (o["params"] = params)  # note these will get serialized by the outer call
    isnothing(data) || (o["data"] = data)
    isnothing(filename) || (o["filename"] = filename)
    isnothing(timeout) || (o["timeout"] = timeout)
    o
end

function resolvename(api::MastAPI, ::Type{T}, name::AbstractString; query_kwargs=(;), kw...) where {T}
    params = (input=name, format="json")
    query(api, T, request("Mast.Name.Lookup"; params, kw...); query_kwargs...)
end
function resolvename(api::MastAPI, name::AbstractString; kw...)
    resolvename(api, Dict, name; kw...)["resolvedCoordinate"][1]
end

function listmissions(api::MastAPI, T::Union{Nothing,Type}; query_kwargs=(;), kw...)
    query(api, T, request("Mast.Missions.List"; kw...); query_kwargs...)
end
listmissions(api::MastAPI; kw...) = listmissions(api, nothing; kw...)

function conesearch(api::MastAPI, T::Union{Nothing,Type}, service::AbstractString,
                    ra::Real, dec::Real, radius::Real=0.2;
                    nr::Union{Nothing,Real}=nothing, ni::Union{Nothing,Real}=nothing,
                    exclude_hla::Bool=false, query_kwargs=(;), kw...)
    params = Dict{Symbol,Real}(:ra=>ra, :dec=>dec, :radius=>radius)
    isnothing(nr) || (params[:nr] = nr)
    isnothing(ni) || (params[:ni] = ni)
    query(api, T, request(service; params, kw...); query_kwargs...)
end
function conesearch(api::MastAPI, service::AbstractString, ra::Real, dec::Real, radius::Real=0.2; kw...)
    conesearch(api, nothing, service, ra, dec, radius; kw...)
end

conesearch_service(::Val{:CAOM}) = "Mast.Caom.Cone"
conesearch_service(::Val{:VO}) = "Vo.Hesarc.DatascopeListable"
conesearch_service(::Val{:HSC_V2}) = "Mast.Hsc.Db.v2"
conesearch_service(::Val{:HSC_V3}) = "Mast.Hsc.Db.v3"
conesearch_service(::Val{:GAIA_DR1}) = "Mast.Catalogs.GaiaDR1.Cone"
conesearch_service(::Val{:GAIA_DR2}) = "Mast.Catalogs.GaiaDR2.Cone"
conesearch_service(::Val{:TGAS}) = "Mast.Catalogs.Tgas.Cone"
conesearch_service(::Val{:GALEX}) = "Mast.Galex.Catalog"
conesearch_service(::Val{:TIC}) = "Mast.Catalogs.Tic.Cone"
conesearch_service(::Val{:CTL}) = "Mast.Catalogs.Ctl.Cone"
conesearch_service(::Val{:DiskDetective}) = "Mast.Catalogs.DiskDetective.Cone"

conesearch_service(s::Symbol) = conesearch_service(Val(s))

function conesearch(api::MastAPI, service::Union{Symbol,Val}, ra::Real, dec::Real, radius::Real=0.2; kw...)
    conesearch(api, conesearch_service(service), ra, dec, radius; kw...)
end

urlof_download(api::MastAPI) = joinpath(api.url, "v0.1/Download/")


export MastAPI


end
