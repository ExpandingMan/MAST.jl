using MAST, HTTP, JSON3, DataFrames
using MAST: query, query_raw, resolvename, listmissions, conesearch


function testsearch(api::MastAPI)
    conesearch(api, "Mast.Caom.Cone", 254.28746, -4.09933, 0.2)
end


scrap() = quote
    api = MastAPI()

    r = Dict("service"=>"Mast.Missions.List",
             "params"=>Dict(),
             "format"=>"json",
            )
end
